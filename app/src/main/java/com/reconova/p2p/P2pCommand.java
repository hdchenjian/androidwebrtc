package com.reconova.p2p;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

import com.reconova.p2p.entity.AirConditionState;
import com.reconova.p2p.entity.FileInfo;

import com.reconova.webrtcclient.WebRtcClient;

/**
 * 用来发送p2p命令，并解析接收到的p2p数据,通过OnP2pCallback将这些数据向上层传递。
 * 
 */
public class P2pCommand {
	
	public static String TYPE_VIDEO_ALL_QUERY = "video_all";
	public static String TYPE_VIDEO_ALARM_QUERY = "video_alarm";
	public static String TYPE_VIDEO_NORMAL_QUERY = "video_normal";
	public static String TYPE_IMAGE = "image";

	private WebRtcClient mRecoLive;
	private OnP2pCallback mOnMsgCallback;

	public P2pCommand(WebRtcClient recoLive, OnP2pCallback onP2pCallback) {
		mRecoLive = recoLive;
		mOnMsgCallback = onP2pCallback;
	}
	
	/**
	 * 对当前直播或回放的视频帧进行快拍抓取，以jpg格式保存到指定的路径上。
	 * 如果保存成功将会回调onSnapShot。
	 * @param jpgPath 图片保存路径
	 */
	public boolean snapShot(String jpgPath) {
		// TODO:
		//return mRecoLive.snapShot(jpgPath);
        return false;
	}
	
	/**
	 * 使能发送debug信息。
	 * @return
	 */
	public boolean enableSendDebug() {
		return mRecoLive.sendMessage("enable_send_debug");
	}
	
	/**
	 * 关闭发送debug信息。
	 * @return
	 */
	public boolean disableSendDebug() {
		return mRecoLive.sendMessage("disable_send_debug");
	}
	
	/**
	 * 更改分辨率
	 * @param width 宽
	 * @param height 高
	 * @return 是否发送成功
	 */
	public boolean changeResolution(int width, int height) {
		StringBuilder builder = new StringBuilder();
		builder.append("change_resolution?")
				.append("width=")
				.append(width)
				.append("&height=")
				.append(height);
		return mRecoLive.sendMessage(builder.toString());
	}
	
	/**
	 * 开始直播
	 * @return
	 */
	public boolean startVideoLive() {
		return mRecoLive.sendMessage("start_video_live");
	}
	
	/**
	 * 关闭直播
	 * @return
	 */
	public boolean stopVideoLive() {
		return mRecoLive.sendMessage("stop_video_live");
	}
	
	/**
	 * 开始本地录像
	 * @return
	 */
	public boolean startRecordVideo() {
		return mRecoLive.sendMessage("start_record");
	}
	
	/**
	 * 关闭本地录像
	 * @return
	 */
	public boolean stopRecordVideo() {
		return mRecoLive.sendMessage("stop_record");
	}
	
	/**
	 * 开始离家模式
	 * @return
	 */
	public boolean startOutSideMode() {
		return mRecoLive.sendMessage("start_outside_mode");
	}
	
	/**
	 * 关闭离家模式
	 * @return
	 */
	public boolean stopOutSideMode() {
		return mRecoLive.sendMessage("stop_outside_mode");
	}
	
	/**
	 * 开启风吹人
	 * @return
	 */
	public boolean windBlowPerson() {
		return mRecoLive.sendMessage("wind_blow_person");
	}
	
	/**
	 * 开启风避人
	 * @return
	 */
	public boolean windAvoidPerson() {
		return mRecoLive.sendMessage("wind_avoid_person");
	}
	
	/**
	 * 自动补光
	 * @return 
	 */
	public boolean autoInfraredLight() {
		return mRecoLive.sendMessage("infrared_light_auto");
	}
	
	/**
	 * 强制开启补光灯
	 * @return
	 */
	public boolean infraredLightOpen() {
		return mRecoLive.sendMessage("infrared_light_open");
	}
	
	/**
	 * 强制关闭补光灯
	 * @return
	 */
	public boolean infraredLightClose() {
		return mRecoLive.sendMessage("infrared_light_close");
	}
	
	/**
	 * 删除文件
	 * @param fileName
	 * @return
	 */
	public boolean fileDelete(String fileName) {
		StringBuilder builder = new StringBuilder();
		builder.append("delete_file?filename=");
		builder.append(fileName);
		return mRecoLive.sendMessage(builder.toString());
	}
	
	/**
	 * 请求下载文件。
	 * @param fileName 要下载文件的文件名
	 * @param savePath 要下载文件在本地的决定路径
	 * @return 0发送请求成功，其他请求失败
	 */
	public int fileRequestDownload(String fileName, String savePath) {
		return mRecoLive.requestGetFile(fileName, savePath);
		//return mRecoLive.requestPutFile("/storage/sdcard0/DCIM/Camera", "7_2017-03-25_15-28-01_T_6.mp4");
	}
	
	/**
	 * 取消下载
	 */
	public void fileCancelDownload() {
		mRecoLive.cancelFile();
	}
	
	/**
	 * 查询文件按信息
	 * @param top 取前几个
	 * @param skip 跳过几个开始取
	 * @param type 文件类型
	 * @return
	 */
	public boolean fileQuery(int top, int skip, String type) {
		StringBuilder builder = new StringBuilder();
		builder.append("file_query?");
		builder.append("top=");
		builder.append(top);
		builder.append("&");
		builder.append("skip=");
		builder.append(skip);
		builder.append("&");
		builder.append("type=");
		builder.append(type);
		return mRecoLive.sendMessage(builder.toString());
	}
	
	/**
	 * 请求回放视频
	 * @param fileName 要回放的视频名
	 * @return
	 */
	public boolean videoReplay(String fileName) {
		StringBuilder builder = new StringBuilder();
		builder.append("video_replay?video=");
		builder.append(fileName);
		return mRecoLive.sendMessage(builder.toString());
	}
	
	/**
	 * 停止回放视频
	 * @param fileName 要停止回放的视频名
	 * @return
	 */
	public boolean stopVideoReplay(String fileName) {
		StringBuilder builder = new StringBuilder();
		builder.append("stop_video_replay?video=");
		builder.append(fileName);
		return mRecoLive.sendMessage(builder.toString());
	}
	
	/**
	 * 解析p2p命令。
	 * @param msg
	 * @return
	 */
	public boolean parse(String msg) {
		try {
			JSONObject json = new JSONObject(msg);
			if (parseState(json)) return true;
			if (parseFileInfos(json)) return true;
			if (parseVideoReplayError(json)) return true;
			if (parseAlarm(json)) return true;
			if (parseAlarmFinish(json)) return true;
			if (parseDebug(json)) return true;
			if (parseDeleteOperation(json)) return true;
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	private boolean parseState(JSONObject json) throws JSONException {
		String action = json.getString("act");
		if (action.trim().equals("state")) {
			AirConditionState state = new AirConditionState();
			state.setInOutSideMode(json.getBoolean("outside"));
			state.setVideoLiving(json.getBoolean("video_live"));
			state.setEnableSendDebug(json.getBoolean("send_debug"));
			state.setVideoRecording(json.getBoolean("video_record"));
			state.setVersionId(json.getString("version_id"));
			String fileName = json.getString("video_replay");
			if (!TextUtils.isEmpty(fileName)) {
				state.setVideoReplaying(true);
				state.setVideoName(fileName);
			} else {
				state.setVideoReplaying(false);
				state.setVideoName(fileName);
			}
			if (json.has("p2p_enable")) {
				state.setP2PEnable(json.getBoolean("p2p_enable"));
			} else {
				state.setP2PEnable(true);
			}
			if (json.has("resolution")) {
				String resolution = json.getString("resolution");
				String[] sizes = resolution.split("X");
				int width = Integer.parseInt(sizes[0]);
				int height = Integer.parseInt(sizes[1]);
				state.setResolution(width, height);
			}
			if (json.has("lighting_mode")) {
				String mode = json.getString("lighting_mode");
				if (mode.trim().equals("auto")) state.setLightMode(AirConditionState.LIGHTING_AUTO);
				if (mode.trim().equals("open")) state.setLightMode(AirConditionState.LIGHTING_OPEN);
				if (mode.trim().equals("close")) {
					state.setLightMode(AirConditionState.LIGHTING_CLOSE);
				}
			}
			mOnMsgCallback.onStateChanged(state);
			return true;
		}
		return false;
	}
	
	private boolean parseFileInfos(JSONObject json) throws JSONException {
		String action = json.getString("act");
		if (action.trim().equals("file_query")) {
			ArrayList<FileInfo> fileInfos = new ArrayList<FileInfo>();
			JSONArray jsonArray = json.getJSONArray("files");
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				FileInfo fileInfo = new FileInfo();
				fileInfo.setId(jsonObject.getString("id"));
				fileInfo.setTime(jsonObject.getString("time"));
				fileInfo.setFileName(jsonObject.getString("filename"));
				fileInfo.setType(jsonObject.getString("type"));
				fileInfos.add(fileInfo);
			}
			mOnMsgCallback.onFileInfo(fileInfos);
			return true;
		}
		return false;
	}
	
	private boolean parseDeleteOperation(JSONObject json) throws JSONException {
		String action = json.getString("act");
		if (action.equals("delete_file")) {
			String filename = json.getString("filename");
			boolean success = json.getBoolean("result");
			String msg = "";
			if (json.has("msg")) {
				msg = json.getString("msg");
			}
			mOnMsgCallback.onDeleteFileResult(success, filename, msg);
			return true;
		} 
		return false;
	}
	
	private boolean parseVideoReplayError(JSONObject json) throws JSONException {
		String action = json.getString("act");
		if (json.has("ret") && action.equals("video_replay")) {
			int ret = json.getInt("ret");
			String msg = "error";
			if (ret == -1) {
				msg = "the video is not exist!";
			} else if (ret == -2){
				msg = "error : some video is replaying!";
			}
			mOnMsgCallback.onError(action, msg);
		} else if (json.has("ret")) {
			String msg = "Operation:" + action + " errors";
			mOnMsgCallback.onError(action, msg);
		}
		return false;
	}
	
	private boolean parseAlarm(JSONObject json) throws JSONException {
		String action = json.getString("act");
		if (action.trim().equalsIgnoreCase("alarm")) {
			mOnMsgCallback.onAlarm();
			return true;
		}
		return false;
	}
	
	private boolean parseAlarmFinish(JSONObject json) throws JSONException {
		String action = json.getString("act");
		if (action.trim().equalsIgnoreCase("alarm_finish")) {
			mOnMsgCallback.onAlarmFinish();
			return true;
		}
		return false;
	}
	
	private boolean parseDebug(JSONObject json) throws JSONException {
		String action = json.getString("act");
		if (action.trim().equalsIgnoreCase("debug")) {
			String msg = json.getString("msg");
			mOnMsgCallback.onDebugMsg(msg);
			return true;
		}
		return false;
	}
	
	/**
	 * 将解析到的命令回调给上层
	 */
	public interface OnP2pCallback {
        /**
         * 收到服务端消息
         * @param content 消息内容
         */
        public void onMessageToast(String content);

		/**
		 * 收到空调状态变化信息
		 * @param state 空调状态
		 */
		public void onStateChanged(AirConditionState state);
		
		/**
		 * 接收到文件信息
		 * @param fileInfos 查询录制视频信息
		 */
		public void onFileInfo(List<FileInfo> fileInfos);
		
		/**
		 * 是否删除文件成功
		 * @param success
		 * @param fileName
		 * @param msg
		 */
		public void onDeleteFileResult(boolean success, String fileName, String msg);
		
		/**
		 * 收到Debug信息
		 * @param msg 
		 */
		public void onDebugMsg(String msg);
		
		/**
		 * 收到错误信息
		 * @param act 对应命令动作
		 * @param msg 消息
		 */
		public void onError(String act, String msg);
		
		/**
		 * 收到陌生人告警信息。
		 */
		public void onAlarm();
		
		/**
		 * 告警信息结束。
		 */
		public void onAlarmFinish();
		
	}

}
