package com.reconova.p2p;

/**
 * Created by immars on 11/2/15.
 */
public class RecoLive {
    /**
     * 自动网络模式
     */
    public static int SERVER_NET_AUTO = 0;
    /**
     * rely模式.
     */
    public static int SERVER_NET_REPLY_MODE = 2;


    public interface RecoLiveEvent {

        /**
         * 成功登陆 .
         */
        public void onLogin();

        /**
         * 登出
         */
        public void onLogout();

        /**
         * p2p连接验证结果回调^M
         *
         * @param result 是否验证成功^M
         * @param msg    验证信息^M
         */
        public void onAuth(boolean result, String msg);

        /**
         * 连接到采集端
         */
        public void onConnect();

        /**
         * 从采集端断开
         */
        public void onDisconnected();

        /**
         * 采集端不在线（可能采集端没连上或者本地网络不通）
         */
        public void onOffLine();

        /**
         * 收到通知
         */
        public void onNotify(String msg);

        /**
         * 对端请求接收文件
         *
         * @param fileName 文件名
         */
        public void onFilePutRequest(String fileName);

        /**
         * 接收文件
         */
        public void onFileAccept();

        /**
         * 下载文件大小为０
         */
        public void onFileSizeError();

        /* toast a message*/
        public void onMessage(String message);


        /**
         * 文件取消传输
         */
        public void onFileAbort();

        /**
         * 文件接收结束
         */
        public void onFileFinish();

        /**
         * 开始文件接收
         */
        public void onFileReject();

        /**
         * 文件传输进度
         *
         * @param localPath    本地保存路径
         * @param totalBytes   文件总大小
         * @param receiveBytes 已经接收的字节
         */
        public void onFileProgress(String localPath, long totalBytes, long receiveBytes);

        /**
         * 视频快拍(截图)所保存图片路径。
         *
         * @param path 图片保存路径
         */
        public void onSnapShot(String path);


        public void onDeviceIsOnline(String deviceId, boolean isOnline);
    }

}
