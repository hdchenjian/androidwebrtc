package com.reconova.p2p.entity;

import java.util.Arrays;

/**
 * 用来记录远端空调当前状态。
 * 
 */
public class AirConditionState {
	public static final int LIGHTING_AUTO = 0; // 自动补光模式
	public static final int LIGHTING_OPEN = 1; // 强制开补光模式
	public static final int LIGHTING_CLOSE = 2; // 强制关补光模式
	
	private boolean isP2PEnalbe = true;
	private boolean isVideoLiving = false; // 是否正在直播
	private boolean isInOutSideMode = false; // 是否处于离家模式
	private boolean isVideoReplaying = false; // 是否正在回放
	private boolean isVideoRecording = false;  // 是否正在录制视频
	private boolean enableSendDebug = false;  // 是否允许发送debug信息
	private String videoName = "";	// 当前直播的视频名,如果没有为空字符。
	private String versionId = "";  // 对应的版本号
	private int lightMode = LIGHTING_AUTO; // 0: auto, 1: open, 2:close
	private int resolution[] = new int[2];
	
	public void clearState() {
		isP2PEnalbe = true;
		isVideoLiving = false;
		isInOutSideMode = false;
		isVideoReplaying = false;
		isVideoRecording = false;
		enableSendDebug = false;
		videoName = "";
		versionId = "";
		resolution[0] = 0;
		resolution[1] = 0;
	}
	
	/**
	 * 智能板是否可以使用p2p
	 * @return
	 */
	public boolean isP2PEnable() {
		return isP2PEnalbe;
	}
	
	/**
	 * 设置p2p是否可以使用。
	 * @param isEnable
	 */
	public void setP2PEnable(boolean isEnable) {
		isP2PEnalbe = isEnable;
	}
	
	/**
	 * 是否正在录制视频 
	 * @return
	 */
	public boolean isVideoRecording() {
		return isVideoRecording;
	}

	/**
	 * 设置是否正在录制视频
	 * @param isVideoRecording
	 */
	public void setVideoRecording(boolean isVideoRecording) {
		this.isVideoRecording = isVideoRecording;
	}

	/**
	 * 是否允许发送debug信息
	 * @return
	 */
	public boolean isEnableSendDebug() {
		return enableSendDebug;
	}

	/**
	 * 设置发送是否允许发送debug信息
	 * @param enableSendDebug
	 */
	public void setEnableSendDebug(boolean enableSendDebug) {
		this.enableSendDebug = enableSendDebug;
	}

	/**
	 * 获取系统版本号
	 * @return
	 */
	public String getVersRionId() {
		return versionId;
	}

	/**
	 * 设置系统版本号
	 * @param versionId
	 */
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	/**
	 * 视频是否正在回放
	 * @return
	 */
	public boolean isVideoReplaying() {
		return isVideoReplaying;
	}

	/**
	 * 设置视频回放状态
	 * @param isVideoReplaying
	 */
	public void setVideoReplaying(boolean isVideoReplaying) {
		this.isVideoReplaying = isVideoReplaying;
	}

	/**
	 * 获取正在回放的视频名
	 * @return
	 */
	public String getVideoName() {
		return videoName;
	}

	/**
	 * 设置正在回放的视频名
	 * @param videoName
	 */
	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}

	/**
	 * 空调是否进行直播。
	 * 
	 * @return
	 */
	public boolean isVideoLiving() {
		return isVideoLiving;
	}

	/**
	 * 设置空调直播状态。
	 * 
	 * @param isVideoLiving
	 */
	public void setVideoLiving(boolean isVideoLiving) {
		this.isVideoLiving = isVideoLiving;
	}

	/**
	 * 空调是否正处于离家模式。
	 */
	public boolean isInOutSideMode() {
		return isInOutSideMode;
	}

	/**
	 * 设置空调是否正处于离家模式。
	 * 
	 * @param isInOutSideMode
	 */
	public void setInOutSideMode(boolean isInOutSideMode) {
		this.isInOutSideMode = isInOutSideMode;
	}
	
	/**
	 * 获取分辨率
	 * 
	 * @return 分辨率  [0]:width, [1]:height
	 */
	public int[] getResolution() {
		return resolution;
	}
	
	
	
	/**
	 * 获取补光等模式
	 * @return
	 */
	public int getLightMode() {
		return lightMode;
	}

	/**
	 * 设置补光灯模式
	 * @param lightMode
	 */
	public void setLightMode(int lightMode) {
		this.lightMode = lightMode;
	}

	/**
	 * 设置分辨率
	 * @param width
	 * @param height
	 */
	public void setResolution(int width, int height) {
		resolution[0] = width;
		resolution[1] = height;
	}

	@Override
	public String toString() {
		return "AirConditionState [isVideoLiving=" + isVideoLiving
				+ ", isInOutSideMode=" + isInOutSideMode
				+ ", isVideoReplaying=" + isVideoReplaying
				+ ", isVideoRecording=" + isVideoRecording
				+ ", enableSendDebug=" + enableSendDebug + ", videoName="
				+ videoName + ", versionId=" + versionId + ", lightMode="
				+ lightMode + ", resolution=" + Arrays.toString(resolution)
				+ "]";
	}
	
}
