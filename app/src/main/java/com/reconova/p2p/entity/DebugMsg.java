package com.reconova.p2p.entity;

import java.util.LinkedList;
import java.util.List;

/**
 * 管理debug信息
 *
 */
public class DebugMsg {
	private int mMaxCount = 20;
	private List<String> mMessages = new LinkedList<String>();
	
	public DebugMsg(int count) {
		mMaxCount = count;
	}
	
	public void clear() {
		mMessages.clear();
	}
	
	public void appendMsg(String msg) {
		if (mMessages.size() == mMaxCount) {
			mMessages.remove(mMaxCount - 1);
		}
		mMessages.add(0, msg);
	}
	
	public String getContent() {
		StringBuilder builder = new StringBuilder();
		for (String msg : mMessages) {
			builder.append(msg);
			if (!msg.endsWith("\n")) {
				builder.append("\n");
			}
		}
		return builder.toString();
	}

}
