package com.reconova.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
	
	public static void create(String filePath) {
		File file = new File(filePath);
		if (!file.exists()) {
			file.mkdirs();
		}
	}
	
	public static List<String> list(String filePath) {
		File file = new File(filePath);
		String[] files = file.list();
		List<String> list = new ArrayList<String>();
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				list.add(files[i]);
			}
		}
		return list;
	}
	
}
