package com.reconova.utils;

import com.reconova.webrtc.VideoActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class VideoUtils {

	public static void showVideoIntent(Activity activity, String filePath) {
		Uri uri = Uri.parse("file://" + filePath);
		// 调用系统自带的播放器
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(uri, "video/mp4");
		activity.startActivity(intent);
	}

	public static void playVideo(Activity activity, String filePath) {
		Intent intent = new Intent(activity, VideoActivity.class);
		Bundle bundle = new Bundle();
		bundle.putString("videoPath", filePath);
		intent.putExtras(bundle);
		activity.startActivity(intent);
	}

}
