package com.reconova.webrtc;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONException;
import org.webrtc.EglBase;
import org.webrtc.MediaStream;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoRenderer;
import org.webrtc.RendererCommon.ScalingType;

import com.reconova.p2p.P2pCommand;
import com.reconova.p2p.entity.AirConditionState;
import com.reconova.p2p.entity.DebugMsg;
import com.reconova.p2p.entity.FileInfo;
import com.reconova.utils.FileUtils;
import com.reconova.utils.NetworkUtil;
import com.reconova.utils.VideoUtils;
import com.reconova.webrtcclient.PercentFrameLayout;
import com.reconova.widget.RecoAutoCompleteTextView;

import com.reconova.p2p.RecoLive.RecoLiveEvent;
import com.reconova.upgrade.UpgradeAction.CheckUpgradeResult;

import com.reconova.webrtcclient.WebRtcClient;
import com.reconova.webrtcclient.PeerConnectionParameters;

import com.reconova.upgrade.UpgradeAction;
import com.reconova.upgrade.RequestResult;
import com.reconova.http.HttpException;
import com.reconova.auth.AuthAction;
import com.reconova.auth.TokenResult;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity  implements WebRtcClient.RtcListener {
    private final static String TAG = MainActivity.class.getCanonicalName();
    private SurfaceViewRenderer remoteRender = null;
    private PercentFrameLayout mView = null;
    private static final int REMOTE_X = 0;
    private static final int REMOTE_Y = 0;
    private static final int REMOTE_WIDTH = 100;
    private static final int REMOTE_HEIGHT = 100;
    private EglBase rootEglBase;
    ScalingType scalingType = ScalingType.SCALE_ASPECT_FILL;
    private RecoLiveEvent mRecoLiveEvent;

    private RecoAutoCompleteTextView mEdtAddr;
	private RecoAutoCompleteTextView mEditDevID;
    private android.widget.TextView mStateInfo;
    private TextView mP2PEnableTextView;    //P2P状态

    private Button mBtnLogin;
    private Button mBtnLogout;

    private Button mBtnResolutionLow;
    private Button mBtnResolutionMid;
    private Button mBtnResolutionHigh;

    private Button mBtnSendStartVideo;
    private Button mBtnSendStopVideo;
    private Button mBtnSnap;

    private Button mBtnRecord;
    private Button mBtnStopRecord;

    private Button mBtnSendStartLeave;   //离家开启
    private Button mBtnSendStopLeave;    //离家关闭

    private Spinner mTopSpinner;
    private Spinner mSkipSpinner;
    private Spinner mTypeSpinner;
    private Spinner mFileSpinner;

    private TextView mFileDetailTextView;

    private Button mBtnDownload;
    private Button mBtnCancel;
    private Button mBtnDeleteFile;

    private Button mBtnQuery;
    private Button mBtnReplay;      //回放
    private Button mBtnStopReplay;

    private Spinner mDownloadSpinner;
    private TextView downloadTextView;

    private Button mBtnPlayLocalVideo;

    private Button mLogButton;

    private Button mBtnAutoInfrared;
	private Button mBtnInfraredOpen;
	private Button mBtnInfraredClose;

	private Button mBtnBlowPerson;
	private Button mBtnAvoidPerson;

	private Button mBtnEnableDebug;
	private Button mBtnDisableDebug;

    private TextView mBlackListTextView;
    private TextView mDebugInfoTextView;

    private Button mBtnCheckUpgrade;
    private Button mBtnConfirmUpgrade;

    private Button mBtnGetToken;


	private List<FileInfo> mQueryFiles = new LinkedList<FileInfo>();
	private ArrayAdapter<FileInfo> mQueryFileAdapter;

	private ArrayAdapter<String> mDownloadAdapter;
	private List<String> mDownloadFiles = new ArrayList<String>();
	private String mDownloadPath; // 用来保存下载文件

	private Dialog mDialog;

	private String mDevID = "";
	private WebRtcClient mLive = WebRtcClient.getInstance();
	private P2pCommand mP2pCommand;

	private AirConditionState mAirconditionState = new AirConditionState();
	private DebugMsg mDebugMsg = new DebugMsg(20);
	private boolean mIsPlayingVideo = false;
	private boolean nScreenON = true;
	
	private CheckUpgradeResult mCheckUpdateResult;

    protected void toast(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

	public void alert(String sTitle, String sMsg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(sTitle);
		builder.setMessage(sMsg);
		builder.setPositiveButton("OK", null);
		builder.show();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().setFormat(PixelFormat.TRANSLUCENT);  
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		mStateInfo = (TextView) findViewById(R.id.textViewInfo);
		mP2PEnableTextView = (TextView) findViewById(R.id.textview_p2p_enable);

		mEdtAddr = (RecoAutoCompleteTextView) findViewById(R.id.editAddr);
		mEdtAddr.initSourePreference(this, "host_key");
		mEdtAddr.showHistoryText();
		mEdtAddr.setThreshold(0);

		mEditDevID = (RecoAutoCompleteTextView) findViewById(R.id.editDevID);
		mEditDevID.initSourePreference(this, "deviceId_key");
		mEditDevID.showHistoryText();
		mEditDevID.setThreshold(0);

        mBtnLogin = (Button) findViewById(R.id.btnLogin);
        mBtnLogin.setOnClickListener(mOnClick);
		mBtnLogout = (Button) findViewById(R.id.btnLogout);
		mBtnLogout.setOnClickListener(mOnClick);

		mBtnResolutionLow = (Button) findViewById(R.id.btnResolutionLow);
		mBtnResolutionLow.setOnClickListener(mOnClick);
		mBtnResolutionMid = (Button) findViewById(R.id.btnResolutionMid);
		mBtnResolutionMid.setOnClickListener(mOnClick);
		mBtnResolutionHigh = (Button) findViewById(R.id.btnResolutionHigh);
		mBtnResolutionHigh.setOnClickListener(mOnClick);

		mBtnSendStartVideo = (Button) findViewById(R.id.btnSendStartVideo);
		mBtnSendStartVideo.setOnClickListener(mOnClick);
		mBtnSendStopVideo = (Button) findViewById(R.id.btnSendStopVideo);
		mBtnSendStopVideo.setOnClickListener(mOnClick);
		mBtnSnap = (Button) findViewById(R.id.btnSnap);
		mBtnSnap.setOnClickListener(mOnClick);

        mBtnRecord = (Button) findViewById(R.id.btnRecord);
        mBtnRecord.setOnClickListener(mOnClick);
        mBtnStopRecord = (Button) findViewById(R.id.btnStopRecord);
        mBtnStopRecord.setOnClickListener(mOnClick);

        mBtnSendStartLeave = (Button) findViewById(R.id.btnLeave);
        mBtnSendStartLeave.setOnClickListener(mOnClick);
        mBtnSendStopLeave = (Button) findViewById(R.id.btnLeaveStop);
        mBtnSendStopLeave.setOnClickListener(mOnClick);

        mBtnDownload = (Button) findViewById(R.id.btnDowload);
        mBtnDownload.setOnClickListener(mOnClick);
        mBtnCancel = (Button) findViewById(R.id.btnCancelDownload);
        mBtnCancel.setOnClickListener(mOnClick);
        mBtnDeleteFile = (Button) findViewById(R.id.btnDelete);
        mBtnDeleteFile.setOnClickListener(mOnClick);

        mBtnQuery = (Button) findViewById(R.id.btnQuery);
        mBtnQuery.setOnClickListener(mOnClick);
        mBtnReplay = (Button) findViewById(R.id.btnReplay);
		mBtnReplay.setOnClickListener(mOnClick);
		mBtnStopReplay = (Button) findViewById(R.id.btnRepayStop);
		mBtnStopReplay.setOnClickListener(mOnClick);

        mBtnPlayLocalVideo = (Button) findViewById(R.id.btnPlayLocal);
        mBtnPlayLocalVideo.setOnClickListener(mOnClick);

        mLogButton = (Button) findViewById(R.id.btnLog);
        mLogButton.setOnClickListener(mOnClick);

        mBtnAutoInfrared = (Button) findViewById(R.id.btnAutoInfrared);
        mBtnAutoInfrared.setOnClickListener(mOnClick);
        mBtnInfraredOpen = (Button) findViewById(R.id.btnOpenInfrared);
        mBtnInfraredOpen.setOnClickListener(mOnClick);
        mBtnInfraredClose = (Button) findViewById(R.id.btnCloseInfrared);
        mBtnInfraredClose.setOnClickListener(mOnClick);

        mBtnBlowPerson = (Button) findViewById(R.id.btnBlowPerson);
        mBtnBlowPerson.setOnClickListener(mOnClick);
        mBtnAvoidPerson = (Button) findViewById(R.id.btnAvoidPerson);
        mBtnAvoidPerson.setOnClickListener(mOnClick);

        mBtnEnableDebug = (Button) findViewById(R.id.btnEnbleDebug);
		mBtnEnableDebug.setOnClickListener(mOnClick);
		mBtnDisableDebug = (Button) findViewById(R.id.btnDisEnbleDebug);
		mBtnDisableDebug.setOnClickListener(mOnClick);

		mBlackListTextView = (TextView) findViewById(R.id.blackListInfo);
		mDebugInfoTextView = (TextView) findViewById(R.id.info_textview);

		mFileSpinner = (Spinner) findViewById(R.id.spinner_video);
		mQueryFileAdapter = new ArrayAdapter<FileInfo>(this,
				android.R.layout.simple_spinner_dropdown_item, mQueryFiles);
		mFileSpinner.setAdapter(mQueryFileAdapter);
		mFileSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				mFileDetailTextView.setText("");
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				mFileDetailTextView.setText("没有任何文件 ");
			}

		});
		mFileDetailTextView = (TextView) findViewById(R.id.video_detail_textview);

		mTopSpinner = (Spinner) findViewById(R.id.spinner_top);
		mTopSpinner.setSelection(0);
		mSkipSpinner = (Spinner) findViewById(R.id.spinner_skip);
		ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item);
		for (int i = 0; i <= 10000; i++) {
			adapter.add(String.valueOf(i));
		}
		mSkipSpinner.setAdapter(adapter);
		mSkipSpinner.setSelection(0);
		mTypeSpinner = (Spinner) findViewById(R.id.spinner_type);

		mBtnCheckUpgrade = (Button) findViewById(R.id.btnCheckUpgrade);
		mBtnCheckUpgrade.setOnClickListener(mOnClick);
		mBtnConfirmUpgrade = (Button) findViewById(R.id.btnConfirmUpgrade);
		mBtnConfirmUpgrade.setOnClickListener(mOnClick);

		mBtnGetToken = (Button) findViewById(R.id.btnGetToken);
		mBtnGetToken.setOnClickListener(mOnClick);

		//mDownloadPath = getExternalCacheDir().getAbsolutePath() + "/render/";
		mDownloadPath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/DCIM/Camera/";
		Log.i("", "Download path:" + mDownloadPath);
		FileUtils.create(mDownloadPath);
		mDownloadSpinner = (Spinner) findViewById(R.id.spinner_local);
		mDownloadAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, mDownloadFiles);
		mDownloadSpinner.setAdapter(mDownloadAdapter);
		updateDownloadInfo();

		downloadTextView = (TextView) findViewById(R.id.textview_download_msg);

		mP2pCommand = new P2pCommand(mLive, new P2pCommand.OnP2pCallback() {

            @Override
            public void onMessageToast(String content) {
                toast(content);
            }

			@Override
			public void onFileInfo(List<FileInfo> fileInfos) {
				mQueryFiles.clear();
				mQueryFiles.addAll(fileInfos);
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        mQueryFileAdapter.notifyDataSetChanged();
                        if (mQueryFiles.isEmpty()) {
                            toast("没有文件返回");
                        } else {
                            toast("返回了" + mQueryFiles.size() + "个文件!");
                        }
                    }
                });

			}

			@Override
			public void onStateChanged(AirConditionState state) {
				mAirconditionState = state;
				StringBuilder builder = new StringBuilder();
				builder.append("直播：" + state.isVideoLiving());
				builder.append(",回放" + state.isVideoReplaying());
				builder.append("离家:" + state.isInOutSideMode());
				updateUIOnUiThread();
			}

			@Override
			public void onError(String act, String msg) {
				toast(msg);
			}

			@Override
			public void onAlarm() {
				if (!mIsPlayingVideo) {
					toast("入侵告警");
					showStrangeDialog();
				} else {
					toast("入侵告警");
				}
			}

			@Override
			public void onAlarmFinish() {
				toast("入侵告警结束");
			}

			@Override
			public void onDebugMsg(String msg) {
				if (msg.contains("blacklist")) {
					toast("进入黑名单" + msg);
					mBlackListTextView.setText("进入黑名单" + msg);
					mBlackListTextView.setVisibility(View.VISIBLE);
					mBlackListTextView.postDelayed(new Runnable() {

						@Override
						public void run() {
							mBlackListTextView.setVisibility(View.GONE);
						}
						
					}, 5 * 1000);
				}
				mDebugMsg.appendMsg(msg);
				updateUIOnUiThread();
			}

			@Override
			public void onDeleteFileResult(boolean success, String fileName,
					String msg) {
				if (success) {
					toast("删除文件成功:" + fileName);
					for (int i = mQueryFiles.size() - 1; i >= 0; i--) {
						FileInfo fileInfo = mQueryFiles.get(i);
						if (fileInfo.getFileName().equals(fileName)) {
							mQueryFiles.remove(i);
							mQueryFileAdapter.notifyDataSetChanged();
							return;
						}
					}
				} else {
					toast("删除文件失败:" + fileName + "," + msg);
				}
			}

		});

		registerReceiver();

        if (remoteRender == null) {
            Log.e(TAG, "creater remoteRender");
            //remoteRender = new SurfaceViewRenderer(this);
            remoteRender = (SurfaceViewRenderer) findViewById(R.id.remote_video_view);
            rootEglBase = EglBase.create();
            remoteRender.init(rootEglBase.getEglBaseContext(), null);
            //remoteRender.setLayoutParams(new LinearLayout.LayoutParams(320, 240));
            remoteRender.setScalingType(scalingType);
            remoteRender.setMirror(false);
            remoteRender.requestLayout();
            mView = (PercentFrameLayout) findViewById(R.id.remote_video_layout);
            //mView.addView(remoteRender, new LinearLayout.LayoutParams(
            //        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            remoteRender.setVisibility(View.VISIBLE);
            updateVideoView();
        }
        mRecoLiveEvent = new RecoLiveEvent() {

            @Override
            public void onLogin() {
                updateUIOnUiThread();
            }

            @Override
            public void onLogout() {
                updateUIOnUiThread();
            }

            @Override
            public void onConnect() {
                updateUIOnUiThread();
                // TODO:
                // Remove this.
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        mBtnSendStartVideo.performClick();
                    }
                });
            }

            @Override
            public void onDisconnected() {
                updateUIOnUiThread();
            }

            @Override
            public void onOffLine() {
                // 本地网络不通，或者采集端不在线。
                updateUIOnUiThread();
            }

            @Override
            public void onNotify(String msg) {
                if (!mP2pCommand.parse(msg)) {
                    toast("json error" + msg);
                    Log.d(TAG, msg);
                }
            }

            @Override
            public void onFilePutRequest(String fileName) {
                // Empty handle
            }

            @Override
            public void onFileAccept() {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        downloadTextView.setText("开始下载文件");
                    }
                });
            }

            @Override
            public void onMessage(String message) {
                toast(message);
            }

            @Override
            public void onFileSizeError() {
                toast("下载失败:下载的文件大小为0");
            }

            @Override
            public void onFileAbort() {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        downloadTextView.setText("文件下载终止");
                    }
                });
            }

            @Override
            public void onFileFinish() {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        downloadTextView.setText("文件下载成功");
                        updateDownloadInfo();                    }
                });
            }

            @Override
            public void onFileReject() {
                toast("对方拒绝了下载请求");
            }

            @Override
            public void onFileProgress(final String locatPath, final long totalBytes,
                                       final long receiveBytes) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        long persent = receiveBytes * 100L / totalBytes;
                        downloadTextView.setText("文件存储位置: " + locatPath +
                                "\n\n共" + totalBytes + "字节, " +
                                "已下载:" + receiveBytes + "字节\n" +
                                "进度" + persent + "%");
                    }
                });

            }

            @Override
            public void onSnapShot(String data) {
                //showImage(mBtnSnap, data);
            }

            @Override
            public void onAuth(boolean result, String msg) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onDeviceIsOnline(String arg0, boolean arg1) {
                // TODO Auto-generated method stub
            }
        };

        updateUIOnUiThread();
	}

    private void updateVideoView() {
        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                mView.setPosition(REMOTE_X, REMOTE_Y, REMOTE_WIDTH, REMOTE_HEIGHT);
                remoteRender.setScalingType(scalingType);
                remoteRender.setMirror(false);
                remoteRender.requestLayout();
            }
        });
    }

	private void updateDownloadInfo() {
		mDownloadFiles.clear();
		mDownloadFiles.addAll(FileUtils.list(mDownloadPath));
		mDownloadAdapter.notifyDataSetChanged();
	}

    private void updateUIOnUiThread() {
        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                updateUI();
            }
        });
    }
    
    private void recoLiveUnInit(boolean cleanAll,
                                boolean videoChannelStop, boolean dataChannelStop) {
        mAirconditionState.clearState();
        mLive.stopWebrtc(videoChannelStop, dataChannelStop);
        if (cleanAll) {
            mLive.unInit();
        }
    }

	private void recoLiveInit(String serverAddress, String peerName) {
        String[] host_port = serverAddress.split(":");
        String ip;
        String port;
        if(host_port.length == 2) {
            ip = host_port[0];
            port = host_port[1];
        } else {
            Log.e(TAG, "server address format error");
            return;
        }
        String socketAddress = "ws://" + ip + ":" + port + "/";
        Log.d(TAG, socketAddress);
        Log.d(TAG, ip);
        mLive.init(this, socketAddress, ip, peerName, mRecoLiveEvent);
	}


    private void recoLiveSart(boolean videoChannelStart, boolean dataChannelStart) {
        Log.d(TAG, "recoLiveSart");
        PeerConnectionParameters params = new PeerConnectionParameters(
                false, false, 0, 0, 15, 1, "H264", true, 1, "opus", true, true);
        mLive.startWebrtc(params, mEditDevID.getText().toString(),
                videoChannelStart, dataChannelStart);
    }

    private void changeDisplaySize(float width, float height) {
        if (remoteRender == null) return;
        float ratio = height / width;
        int h = (int) (ratio * remoteRender.getWidth());
        remoteRender.setLayoutParams(new LinearLayout.LayoutParams(remoteRender.getWidth(), h));
        updateVideoView();
    }

    private void registerReceiver() {
        // 网络状态广播,在断网时候自动销毁p2p相关数据。
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(connectionReceiver, intentFilter);
    }

    /**
     * 断网时关闭清理p2p相关资源, 连网时自动连接。
     */
    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (!NetworkUtil.isConnected(getApplicationContext())) {
                    // 断网处理，清空直播资源。
                    recoLiveUnInit(true, true, true);
                    updateUI();
                    toast("连接网络失败,请检查网络连接!");
                }
            }
	};

    private void showStrangeDialog() {
        if (mDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("发现陌生人");
            builder.setMessage("发现陌生人，要查看录像吗？");
            builder.setNegativeButton("取消",
                                      new DialogInterface.OnClickListener() {

                                          @Override
                                          public void onClick(DialogInterface dialog, int which) {
                                              mDialog.dismiss();
                                          }

                                      });
            builder.setPositiveButton("查看录像",
                                      new DialogInterface.OnClickListener() {

                                          @Override
                                          public void onClick(DialogInterface dialog, int which) {
                                              mDialog.dismiss();
                                              mBtnSendStartVideo.performClick();
                                          }
                                      });
            mDialog = builder.create();
        }

        mDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void onDestroy() {
        if (remoteRender != null) {
            remoteRender.release();
            remoteRender = null;
        }
        unregisterReceiver(connectionReceiver);
        recoLiveUnInit(true, true, true);
        super.onDestroy();
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            showExitDialog();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    public void showExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm");
        builder.setMessage("Are you sure to exit?");
        builder.setPositiveButton("YES", mDlgClick);
        builder.setNegativeButton("NO", mDlgClick);
        builder.show();
    }

    private DialogInterface.OnClickListener mDlgClick = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == AlertDialog.BUTTON_POSITIVE) {
                    recoLiveUnInit(true, true, true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            }
	};

    private View.OnClickListener mOnClick = new View.OnClickListener() {
            public void onClick(View args0) {
                Log.d(TAG, getResources().getResourceEntryName(args0.getId()));
                switch (args0.getId()) {
                    case R.id.btnLogin:
                        if (mLive.isLogined()) {
                            toast("您已经登录了!");
                        } else {
                            mEdtAddr.saveHistory();
                            mEditDevID.saveHistory();
                            mDevID = mEditDevID.getText().toString();
                            recoLiveInit(mEdtAddr.getText().toString(), mDevID);
                        }
                        updateUI();
                        break;

                    case R.id.btnLogout:
                        if (alertLoginUnSuccess()) {
                            return;
                        }
                        recoLiveUnInit(true, true, true);
                        updateUI();
                        break;

                    case R.id.btnSendStartVideo:
                        if (alertLoginUnSuccess()) {
                            return;
                        }
                        if (mAirconditionState.isVideoReplaying()) {
                            toast("正在回放:" + mAirconditionState.getVideoName());
                            return;
                        }
                        mIsPlayingVideo = mLive.startVideo();
                        // 如果空调视频发送状态不是正在发送视频，请求发送视频。
                        if (!mAirconditionState.isVideoLiving()) {
                            recoLiveSart(true, false);
                            if (mP2pCommand.startVideoLive()) {
                                toast("capture send succssfully.");
                            } else {
                                toast("capture send failed.");
                            }
                        }
                        break;

                    case R.id.btnSendStopVideo:
                        if (alertIfUnConnected()) {
                            return;
                        }
                        if (mAirconditionState.isVideoReplaying()) {
                            toast("正在回放:" + mAirconditionState.getVideoName());
                            return;
                        }
                        mIsPlayingVideo = false;
                        if (mP2pCommand.stopVideoLive()) {
                            toast("stop_cap send succssfully.");
                        } else {
                            toast("stop_cap send failed.");
                        }
                        toast("直播已关闭");
                        recoLiveUnInit(false, true, false);
                        break;

                    case R.id.btnRecord:
                        if (alertIfUnConnected()) {
                            return;
                        }
                        mP2pCommand.startRecordVideo();
                        break;

                    case R.id.btnStopRecord:
                        if (alertIfUnConnected()) {
                            return;
                        }
                        mP2pCommand.stopRecordVideo();
                        break;

                    case R.id.btnLeave:
                        if (alertIfUnConnected()) {
                            return;
                        }
                        // 发送开启离家模式。
                        if (mP2pCommand.startOutSideMode()) {
                            toast("outside_mode send succssfully.");
                        } else {
                            toast("outside_mode send failed.");
                        }
                        updateUI();
                        break;

                    case R.id.btnLeaveStop:
                        if (alertIfUnConnected()) {
                            return;
                        }
                        // 发送关闭离家模式。
                        if (mP2pCommand.stopOutSideMode()) {
                            toast("outside_mode stopWebrtc send succssfully.");
                        } else {
                            toast("outside_mode  stopWebrtc send failed.");
                        }
                        updateUI();
                        break;

                    case R.id.btnDowload:
                        if (alertIfUnConnected()) {
                            return;
                        }
                        if (mQueryFiles == null || mQueryFiles.isEmpty()) {
                            toast("文件列表为空!");
                            return;
                        }

                        recoLiveSart(false, true);
                        int location = mFileSpinner.getSelectedItemPosition();
                        String filename = mQueryFiles.get(location).getFileName();
                        int requestCode = mP2pCommand.fileRequestDownload(filename, mDownloadPath);
                        if (requestCode == 0) {
                            toast("发送请求下载文件:" + filename);
                        } else if (requestCode == -1) {
                            toast("文件正在下载");
                        } else {
                        toast("发送请求下载文件失败:" + requestCode);
                        }
                    break;

                case R.id.btnCancelDownload:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    mP2pCommand.fileCancelDownload();
                    toast("已取消下载");
                    break;

                case R.id.btnDelete:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    if (mQueryFiles == null || mQueryFiles.isEmpty()) {
                        toast("文件列表为空!");
                        return;
                    }
                    int position = mFileSpinner.getSelectedItemPosition();
                    mP2pCommand.fileDelete(mQueryFiles.get(position).getFileName());
                    break;

                case R.id.btnQuery:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    int top = 1;
                    int skip = 0;
                    try {
                        top = Integer.parseInt((String) mTopSpinner
                                .getSelectedItem());
                    } catch (Exception e) {
                    }
                    try {
                        skip = Integer.parseInt((String) mSkipSpinner
                                .getSelectedItem());
                    } catch (Exception e) {
                    }
                    String type = (String) mTypeSpinner.getSelectedItem();
                    if (type.equals("所有视频")) {
                        type = P2pCommand.TYPE_VIDEO_ALL_QUERY;
                    }
                    if (type.equals("告警视频")) {
                        type = P2pCommand.TYPE_VIDEO_ALARM_QUERY;
                    }
                    if (type.equals("正常视频")) {
                        type = P2pCommand.TYPE_VIDEO_NORMAL_QUERY;
                    }
                    if (type.equals("图片")) {
                        type = P2pCommand.TYPE_IMAGE;
                    }

                    if (mP2pCommand.fileQuery(top, skip, type)) {
                        toast("查询发送成功!");
                    } else {
                        toast("查询发送失败!");
                    }
                    break;

                case R.id.btnReplay:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    if (mQueryFiles == null || mQueryFiles.isEmpty()) {
                        toast("直播列表为空!");
                        return;
                    }
                    if (mAirconditionState.isVideoLiving()) {
                        toast("正在直播中!");
                    } else if (mAirconditionState.isVideoReplaying()) {
                        mIsPlayingVideo = mLive.startVideo();
                        toast("正在回放:" + mAirconditionState.getVideoName());
                    } else {
                        recoLiveSart(true, false);
                        mIsPlayingVideo = mLive.startVideo();
                        int position_other = mFileSpinner.getSelectedItemPosition();
                        mP2pCommand.videoReplay(mQueryFiles.get(position_other)
                                                .getFileName());
                    }
                    break;

                case R.id.btnRepayStop:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    if (mAirconditionState.isVideoLiving()) {
                        toast("正在直播中!");
                        return;
                    }
                    if (!mAirconditionState.isVideoReplaying()) {
                        toast("没有回放");
                        return;
                    }
                    if (mP2pCommand.stopVideoReplay(mAirconditionState
                                                    .getVideoName())) {
                        toast("发送成功!");
                    } else {
                        toast("发送失败!");
                    }
                    mIsPlayingVideo = false;
                    break;

                case R.id.btnPlayLocal:
                    String fileName = (String) mDownloadSpinner.getSelectedItem();
                    if (fileName == null || fileName.trim().isEmpty()) {
                        toast("没有文件播放");
                        return;
                    }
                    if (!fileName.endsWith(".mp4")) {
                        toast("所选文件不是视频文件!");
                        return;
                    }

                    VideoUtils.playVideo(MainActivity.this, mDownloadPath
                            + fileName);
                    break;

                case R.id.btnEnbleDebug:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    mP2pCommand.enableSendDebug();
                    break;
                case R.id.btnDisEnbleDebug:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    mP2pCommand.disableSendDebug();
                    break;
                case R.id.btnAutoInfrared:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    mP2pCommand.autoInfraredLight();
                    break;
                case R.id.btnCloseInfrared:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    mP2pCommand.infraredLightClose();
                    break;
                case R.id.btnOpenInfrared:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    mP2pCommand.infraredLightOpen();
                case R.id.btnBlowPerson:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    mP2pCommand.windBlowPerson();
                    break;
                case R.id.btnAvoidPerson:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    mP2pCommand.windAvoidPerson();
                    break;

                case R.id.btnResolutionLow:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    changeDisplaySize(320, 240);
                    changeResolution(320, 240);
                    break;
                case R.id.btnResolutionMid:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    changeDisplaySize(640, 480);
                    changeResolution(640, 480);
                    break;
                case R.id.btnResolutionHigh:
                    changeDisplaySize(1280, 720);
                    changeResolution(1280, 720);
                    break;
                case R.id.btnCheckUpgrade:
                    checkUpdrade();
                    break;
                case R.id.btnConfirmUpgrade:
                    confirmUpgrade();
                    break;
                case R.id.btnSnap:
                    snapShot();
                    break;
                case R.id.btnLog:
                    if (alertIfUnConnected()) {
                        return;
                    }
                    mP2pCommand.fileRequestDownload("facedis.log", mDownloadPath);
                    break;
                case R.id.btnGetToken:
                    requestToken();
                    break;
                default:
                    break;
                }
            }
	};
	
    /**
     * 快拍抓取当前视频帧。
     */
    private void snapShot() {
        AsyncTask<Object, Object, Boolean> task = new AsyncTask<Object, Object, Boolean>() {
                private String imagePath;

                @Override
                protected Boolean doInBackground(Object... params) {
                    imagePath = Environment.getExternalStorageDirectory() + "/test.jpg";
                    return mP2pCommand.snapShot(imagePath);
                }

                @Override
                protected void onPostExecute(Boolean result) {
                    if (!result) {
                        toast("图片保存失败:" + imagePath);
                    }
                }
            };
        task.execute();
    }

    private void checkUpdrade() {
        AsyncTask<Object, Object, String> task = new AsyncTask<Object, Object, String>() {
                private ProgressDialog dialog;
			
                @Override
                protected void onPreExecute() {
                    mCheckUpdateResult = null;
                    dialog = new ProgressDialog(MainActivity.this);
                    dialog.setTitle("获取空调是否需要升级信息...");
                    dialog.show();
                }


                @Override
                protected String doInBackground(Object... params) {
                    try {
                        String deviceId = mEditDevID.getText().toString();
                        mCheckUpdateResult = UpgradeAction.checkUpgrade(deviceId);
                        return mCheckUpdateResult.getMessage();
                        // return mResult.errmsg + ":" + mResult.firm_need_update;
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return e.getMessage();
                    } catch (HttpException e) {
                        e.printStackTrace();
                        return e.getMessage();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return e.getMessage();
                    }
                }

                @Override
                protected void onPostExecute(String result) {
                    dialog.dismiss();
                    toast(result);
                }

            };
        task.execute();
    }

    private void confirmUpgrade() {
        AsyncTask<Object, Object, String> task = new AsyncTask<Object, Object, String>() {
                private ProgressDialog dialog;
			
                @Override
                protected void onPreExecute() {
                    dialog = new ProgressDialog(MainActivity.this);
                    dialog.setTitle("确认升级中...");
                    dialog.show();
                }

                @Override
                protected String doInBackground(Object... params) {
                    if (mCheckUpdateResult == null) {
                        return "没有升级信息";
                    }
                    if (!mCheckUpdateResult.isFirmNeedUpdate()) {
                        return "不需要确认升级";
                    }
                    try {
                        String deviceId = mEditDevID.getText().toString();
                        RequestResult result = UpgradeAction.confirmUpgrade(
                            deviceId, mCheckUpdateResult.getFirmTargetVersion());
                        return result.getMessage();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return e.getMessage();
                    } catch (HttpException e) {
                        e.printStackTrace();
                        return e.getMessage();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return e.getMessage();
                    }
                }

                @Override
                protected void onPostExecute(String result) {
                    dialog.dismiss();
                    toast(result);
                }

            };
        task.execute();
    }

    private void changeResolution(int width, int height) {
        if (!mP2pCommand.changeResolution(width, height)) {
            toast("发送改变分辨率失败!");
        } else {
            toast("分辨率更改已发送!");
        }
    }
	
    private void requestToken() {
        AsyncTask<Object, Object, String> task = new AsyncTask<Object, Object, String>() {
                private ProgressDialog dialog;
                private TokenResult tokenResult;
			
                @Override
                protected void onPreExecute() {
                    dialog = new ProgressDialog(MainActivity.this);
                    dialog.setTitle("获取token...");
                    dialog.show();
                }


                @Override
                protected String doInBackground(Object... params) {
                    try {
                        AuthAction authAction = new AuthAction();
                        // TODO: 需要换成你们实际的token Id。
                        tokenResult = authAction.getToken("test_userId"); 
                        if (tokenResult.isSuccessful()) {
                            return "获取token:" + tokenResult.getToken() + "成功!";
                        } else {
                            return tokenResult.getMessage();
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return e.getMessage();
                    } catch (HttpException e) {
                        e.printStackTrace();
                        return e.getMessage();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return e.getMessage();
                    } catch (IOException e) {
                        return  e.getMessage();
                    }
                }

                @Override
                protected void onPostExecute(String result) {
                    dialog.dismiss();
                    toast(result);
                }

            };
        task.execute();
    }

    private boolean alertLoginUnSuccess() {
        if (!mLive.isLogined()) {
            alert("尚未登陆成功", "未登陆成功,请检查网络连接!");
            return true;
        } else {
            return false;
        }
    }

    private boolean alertIfUnConnected() {
        if (!mLive.isConnected()) {
            toast("p2p尚未连接成功！");
            return true;
        } else {
            return false;
        }
    }

    private void updateUI() {
        if (!mLive.isLogined()) {
            mStateInfo.setText(R.string.txtStateNologin);
            mBtnLogin.setVisibility(View.VISIBLE);
        } else {
            mBtnLogin.setVisibility(View.GONE);
            if (mLive.isConnected()) {
                mStateInfo.setText(R.string.txtStateConntected);
            } else {
                mStateInfo.setText(R.string.txtStateNoConntected);
            }
        }
		
        if (mAirconditionState.isP2PEnable()) {
            mP2PEnableTextView.setVisibility(View.GONE);
        } else {
            mP2PEnableTextView.setVisibility(View.VISIBLE);
            mP2PEnableTextView.setText("智能板禁止P2P功能!");
        }

        if (!mLive.isConnected()) {
            mIsPlayingVideo = false;
            mAirconditionState.clearState();
        }

        if (mAirconditionState.isInOutSideMode()) {
            mBtnSendStartLeave.setVisibility(View.GONE);
            mBtnSendStopLeave.setVisibility(View.VISIBLE);
        } else {
            mBtnSendStartLeave.setVisibility(View.VISIBLE);
            mBtnSendStopLeave.setVisibility(View.GONE);
        }
		
        switch (mAirconditionState.getLightMode()) {
        case AirConditionState.LIGHTING_AUTO:
            mBtnAutoInfrared.setTextColor(Color.GREEN);
            mBtnInfraredOpen.setTextColor(Color.BLACK);
            mBtnInfraredClose.setTextColor(Color.BLACK);
            break;
        case AirConditionState.LIGHTING_OPEN:
            mBtnAutoInfrared.setTextColor(Color.BLACK);
            mBtnInfraredOpen.setTextColor(Color.GREEN);
            mBtnInfraredClose.setTextColor(Color.BLACK);
            break;
        case AirConditionState.LIGHTING_CLOSE:
            mBtnAutoInfrared.setTextColor(Color.BLACK);
            mBtnInfraredOpen.setTextColor(Color.BLACK);
            mBtnInfraredClose.setTextColor(Color.GREEN);
            break;
        }

        String versionMsg = "版本号：" + mAirconditionState.getVersRionId() + "\n";
        // String versionMsg = mAirconditionState.toString() + "\n";
        mDebugInfoTextView.setText(versionMsg + mDebugMsg.getContent());
    }


    // implement WebRtcClient.RtcListener
    @Override
    public void onStatusChanged(String newStatus) {
		//toast(newStatus);
    }

    @Override
    public void onAddRemoteStream(MediaStream remoteStream) {
        remoteStream.videoTracks.get(0).addRenderer(new VideoRenderer(remoteRender));
        updateVideoView();
    }

    @Override
    public void onRemoveRemoteStream() {}

    @Override
    public void onPeerLeave(){
        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                /*
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("视频已播放完或连接已断开");
                builder.setTitle("提示");
                builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //android.os.Process.killProcess(android.os.Process.myPid());
                        //restart(getApplicationContext(), 500);
                    }
                });
                builder.create().show();
                */
                toast("视频已播放完或文件下载完成");
                updateUI();
            }
        });
        mIsPlayingVideo = false;
        recoLiveUnInit(false, true, true);
        mAirconditionState.clearState();
        updateVideoView();
        updateUIOnUiThread();
    }
}
