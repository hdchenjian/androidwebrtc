package com.reconova.webrtc;

import java.io.File;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoActivity extends Activity {

	private VideoView video1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video);

		Bundle bundle = this.getIntent().getExtras();
		String videoPath = bundle.getString("videoPath");
		Log.i("获取到的文件名称为 ", videoPath);

		video1 = (VideoView) findViewById(R.id.videoView1);
		MediaController mediaco = new MediaController(this);

		File file = new File(videoPath);
		if (file.exists()) {
			// VideoView与MediaController进行关联
			video1.setVideoPath(file.getAbsolutePath());
			video1.setMediaController(mediaco);
			mediaco.setMediaPlayer(video1);
			// 让VideiView获取焦点
			video1.requestFocus();
		}
	}
}
