package com.reconova.webrtcclient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.lang.Thread;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Timestamp;
import java.net.URI;
import java.net.URISyntaxException;

import android.util.Base64;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.util.Log;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

import org.json.JSONException;
import org.json.JSONObject;

import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RtpReceiver;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;

import com.reconova.p2p.RecoLive.RecoLiveEvent;

public class WebRtcClient {
    private final static String TAG = WebRtcClient.class.getCanonicalName();
    private PeerConnectionFactory factory;
    private LinkedList<PeerConnection.IceServer> iceServers = new LinkedList<>();
    private PeerConnectionParameters peerConnectionParameters;
    private MediaConstraints pcConstraints;
    private RtcListener mListener;
    private WebSocketClient websocketClient;
    private boolean loginInSuccess = false;
    private String userName;
    private String peerName;
    private Peer peer;
    private final ScheduledExecutorService executor;
    private final ScheduledExecutorService executorSaveData;
    private final ScheduledExecutorService executorPing;
    private static final WebRtcClient instance = new WebRtcClient();
    private PeerConnection pc;
    private String turn_key = "t=u408021891333";
    private String host;
    private boolean videoChannelUsed = false;
    private boolean dataChannelUsed = false;
    private DataChannel dataChannelLocal;
    private long timestampWebrtc = 0;

    private String downloadFileName;
    private long downloadFileSize;
    private long downloadFileSizeCurrent = 0;
    private String downloadSavePath;
    private boolean isDownloadFile = false;

    private boolean isUploadFile = false;
    private boolean uploadFileAccepted = false;
    private String uploadFileName;
    private String uploadFilePath;
    private long uploadFileSize;
    private String uploadRealFileSavePath;

    private RecoLiveEvent mRecoLiveEvent;
    private boolean createWebrtcCalled = false;
    public boolean hasCallInit() {return createWebrtcCalled;}

    public boolean isLogined() {return loginInSuccess;}
    public boolean isConnected() {
        return websocketClient != null &&
                websocketClient.getReadyState() == WebSocket.READYSTATE.OPEN;
    }

    public boolean startVideo() {return true;}

    private synchronized boolean getVideoChannelStatus() {
        return videoChannelUsed;
    }
    private synchronized boolean getDataChannelStatus() {
        return dataChannelUsed;
    }
    private synchronized void setVideoChannelStatus(boolean status) {
        videoChannelUsed = status;
        if(!videoChannelUsed && !dataChannelUsed) {
            if(!createWebrtcCalled) {
                return;
            }
            createWebrtcCalled = false;
            cleanFileTransformParameter();
            uploadFileAccepted = true;
            Log.d(TAG, "dispose pc");
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    if (pc != null) {
                        pc.dispose();
                        pc = null;
                    }
                    JSONObject payload = new JSONObject();
                    sendMessage("leave", payload);
                }
            });
        }
    }
    private synchronized void setDataChannelStatus(boolean status) {
        dataChannelUsed = status;
        Log.d(TAG, "dataChannelUsed: " + dataChannelUsed);
        if(!videoChannelUsed && !dataChannelUsed) {
            if(!createWebrtcCalled) {
                return;
            }
            createWebrtcCalled = false;
            cleanFileTransformParameter();
            uploadFileAccepted = true;
            Log.d(TAG, "dispose pc");
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    if (pc != null) {
                        pc.dispose();
                        pc = null;
                    }
                    JSONObject payload = new JSONObject();
                    sendMessage("leave", payload);
                }
            });
        }
    }

    private long getSystemTimestamp() {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        return ts.getTime() / 1000;
    }
    private void cleanFileTransformParameter() {
        Log.d(TAG, "cleanFileTransformParameter");
        downloadFileName = "";
        downloadFileSize = 0;
        downloadFileSizeCurrent = 0;
        isDownloadFile = false;
        downloadSavePath = "";

        uploadRealFileSavePath = "";
        isUploadFile = false;
        uploadFileName = "";
        uploadFilePath = "";
        uploadFileAccepted = false;
    }

    public int requestGetFile(String fileName_, String savePath) {
        if(isDownloadFile || isUploadFile) {
            return -1;
        }
        cleanFileTransformParameter();
        isDownloadFile = true;
        this.downloadSavePath = savePath;
        try {
            JSONObject payload = new JSONObject();
            payload.put("fileName", fileName_);
            payload.put("type", "pull");
            sendMessage("file-transform", payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int requestPutFile(String filePath, String fileName) {
        if(isDownloadFile || isUploadFile) {
            return -1;
        }
        cleanFileTransformParameter();
        isUploadFile = true;
        if(filePath.charAt(filePath.length() - 1) == '/') {
            uploadFilePath = filePath;
        } else {
            uploadFilePath = filePath + "/";
        }
        uploadFileName = fileName;
        try {
            File file = new File(uploadFilePath + uploadFileName);
            if (!file.exists()) {
                mRecoLiveEvent.onMessage("file dose not exists");
                cleanFileTransformParameter();
                return -1;
            }
            long fileSize = file.length();
            this.uploadFileSize = fileSize;
            JSONObject payload = new JSONObject();
            payload.put("fileName", fileName);
            payload.put("fileSize", fileSize);
            payload.put("type", "push");
            sendMessage("file-transform", payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void cancelFile() {
        if(!isDownloadFile && !isUploadFile) {
            return;
        }

        mRecoLiveEvent.onFileAbort();
        Log.d(TAG, "cancelFile");
        JSONObject payload = new JSONObject();
        try {
            if(isDownloadFile) {
                payload.put("type", "pull");
            } else {
                payload.put("type", "push");
            }
            sendMessage("file-transform-cancel", payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        cleanFileTransformParameter();
        uploadFileAccepted = true;
        setDataChannelStatus(false);
    }

    /* Implement this interface to be notified of events. */
    public interface RtcListener{
        void onStatusChanged(String newStatus);
        void onAddRemoteStream(MediaStream remoteStream);
        void onRemoveRemoteStream();
        void onPeerLeave();
    }

    private void handleLogin(JSONObject payload) throws JSONException {
        Log.d(TAG, "handleLogin");
        boolean login_result = payload.getBoolean("success");
        if(login_result == true){
            loginInSuccess = true;
            mRecoLiveEvent.onLogin();
            mRecoLiveEvent.onConnect();
            executorPing.execute(new Runnable() {
                @Override
                public void run() {
                    while(loginInSuccess) {
                        try {
                            if(!isConnected()) {break;}
                            final JSONObject payload = new JSONObject();
                            payload.put("type", "ping-signal");
                            if(getSystemTimestamp() - timestampWebrtc > 15) {
                                Log.d(TAG, getSystemTimestamp() + " : " + timestampWebrtc);
                                setVideoChannelStatus(false);
                                setDataChannelStatus(false);
                            }
                            executor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    websocketClient.send(payload.toString());
                                }
                            });
                            Thread.sleep(10000);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            Log.d(TAG, "LoginCommand: success");
        } else{
            Log.d(TAG, "Ooops...try a different username");
            sendLoginSignal();
            //loginInSuccess = false;
        }
    }

    private void handleOffer(final JSONObject payload) throws JSONException {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    SessionDescription sdp = new SessionDescription(
                            SessionDescription.Type.fromCanonicalForm(payload.getString("type")),
                            payload.getString("sdp")
                    );
                    pc.setRemoteDescription(peer, sdp);
                    pc.createAnswer(peer, pcConstraints);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Log.d(TAG,"handleOffer");
    }

    private void handleAnswer(final JSONObject payload) throws JSONException {
        Log.d(TAG,"handleAnswer");
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    SessionDescription sdp = new SessionDescription(
                            SessionDescription.Type.fromCanonicalForm(payload.getString("type")),
                            payload.getString("sdp")
                    );
                    pc.setRemoteDescription(peer, sdp);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void handleCandidate(final JSONObject payload) throws JSONException {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (pc.getRemoteDescription() != null) {
                        IceCandidate candidate = new IceCandidate(
                                payload.getString("sdpMid"),
                                payload.getInt("sdpMLineIndex"),
                                payload.getString("candidate"));
                        pc.addIceCandidate(candidate);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Log.d(TAG,"handleCandidate");
    }

    private void handleFileTransform(final JSONObject payload) throws JSONException {
        downloadFileName = payload.getString("fileName");
        downloadFileSize = payload.getLong("fileSize");
        mRecoLiveEvent.onFileAccept();
        sendMessage("file-transform-parameter-accept", new JSONObject());
    }

    private void handleLeave() {
        mListener.onPeerLeave();
    }

    /**
     * Send a message through the signaling server
     * @param type type of message
     * @param payload payload of message
     */
    public void sendMessage(String type, JSONObject payload) {
        if(!isConnected()) {
            return;
        }
        try {
            final JSONObject message = new JSONObject();
            String to = peerName;
            message.put("name", to);
            message.put("type", type);
            message.put("payload", payload);
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    websocketClient.send(message.toString());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean sendMessage(String payload) {
        if(!isConnected()) {
            return false;
        }
        try {
            final JSONObject message = new JSONObject();
            String to = peerName;
            message.put("name", to);
            message.put("type", "message");
            message.put("payload", payload);
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    websocketClient.send(message.toString());
                }
            });
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void getMessage(JSONObject data){
        try {
            Log.d(TAG, "videoChannelUsed: " + videoChannelUsed + " dataChannelUsed: " + dataChannelUsed);
            String type = data.getString("type");
            String from = data.optString("from");
            JSONObject payload = null;
            payload = data.optJSONObject("payload");
            // if peer is unknown, try to add him
            if(!from.equals("") && (type.equals("offer") || type.equals("candidate"))) {
                while (pc == null) {
                    Thread.sleep(500);
                    Log.d(TAG,"sleeping");
                }
            }

            switch(type){
            case "login":
                handleLogin(data);
		        break;
            case "offer-loopback-reconnect":
                if(pc == null) {
                    Log.d(TAG, "pc is null \n");
                }
                stopWebrtc(true, true);
                PeerConnectionParameters params = new PeerConnectionParameters(
                        false, false, 0, 0, 15, 1, "H264", true, 1, "opus", true, true);
                startWebrtc(params, from, true, false);
                sendMessage("start_video_live");
                break;
            case "offer":
                handleOffer(payload);
                break;
            case "answer":
                handleAnswer(payload);
                break;
            case "candidate":
                handleCandidate(payload);
                break;
            case "save-data-over":
                uploadFileAccepted = true;
                break;
            case "file-transform-parameter":
                handleFileTransform(payload);
                break;
            case "file-transform-parameter-accept":
                Log.d(TAG, "file-transform-parameter-accept " + dataChannelLocal.state() + isUploadFile + " " + getDataChannelStatus());
                if (dataChannelLocal.state() == DataChannel.State.OPEN &&
                        isUploadFile && getDataChannelStatus()) {
                    Log.d(TAG, "file-transform-parameter-accept sendData");
                    executorSaveData.execute(new Runnable() {
                        @Override
                        public void run() {
                            sendData();
                        }
                    });
                }
                break;
            case "file-transform-file-size-zero":
                mRecoLiveEvent.onFileSizeError();
                cleanFileTransformParameter();
                break;
            case "file-transform-cancel":
                break;
            case "leave":
                handleLeave();
                break;
            case "message":
                mRecoLiveEvent.onNotify(data.optString("payload"));
                break;
            case "pong-signal":
                timestampWebrtc = getSystemTimestamp();
                break;
            case "peer-excced-3":
                mRecoLiveEvent.onMessage("连接的用户超过3人,请稍后再试");
                break;
            case "peer-not-exist":
                mRecoLiveEvent.onMessage("设备不存在,请重新输入设备名");
                String return_type = data.getString("return_type");
                if(return_type.equals("message")) {
                    String payload_str = data.getString("payload");
                    if(payload_str.equals("start_video_live") ||
                            payload_str.startsWith("video_replay?video=")) {
                        stopWebrtc(true, true);
                    }
                } else if(return_type.equals("file-transform")) {
                    stopWebrtc(true, true);
                } else {
                    Log.d(TAG, "ignore peer-not-exist");
                }
                break;
            case "toast-info":
                System.out.println(payload.toString());
                System.out.println(payload.optString("content"));
                mRecoLiveEvent.onMessage(payload.optString("content"));
                break;
            default:
                Log.d(TAG, "unkonw message type");
                break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch(InterruptedException e){
            e.printStackTrace();
        }
    }


    private class Peer implements SdpObserver, PeerConnection.Observer, DataChannel.Observer{
        /* Called on success of Create{Offer,Answer}(). implements SdpObserver interface*/
        @Override
        public void onCreateSuccess(final SessionDescription sdp) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        String sdpString = sdp.description;
                        sdpString = preferCodec(
                                sdpString, peerConnectionParameters.videoCodec, false);
                        if(sdpString.equals("")) {
                            JSONObject payload = new JSONObject();
                            sendMessage("leave", payload);
                            mRecoLiveEvent.onMessage("您的设备不支持视频解码，无法观看视频");
                            mRecoLiveEvent.onDisconnected();
                            stopWebrtc(true, true);
                            return;
                        }
                        //sdpString = preferCodec(
                        //        sdpString, peerConnectionParameters.audioCodec, true);
                        JSONObject payload = new JSONObject();
                        payload.put("type", sdp.type.canonicalForm());
                        payload.put("sdp", sdpString);
                        sendMessage(sdp.type.canonicalForm(), payload);
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                pc.setLocalDescription(Peer.this, sdp);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            Log.d(TAG, "onCreateSuccess");
        }

        @Override
        public void onSetSuccess() {}
        @Override
        public void onCreateFailure(String s) {Log.d(TAG, "onCreateFailure: " + s);}
        @Override
        public void onSetFailure(String s) {}


        /* implement PeerConnection.Observer interface */
        @Override
        public void onSignalingChange(PeerConnection.SignalingState signalingState) {
            Log.d(TAG, "onSignalingChange: " + signalingState.toString());
        }

        @Override
        public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
            Log.d(TAG, iceConnectionState.toString());
            if(iceConnectionState == PeerConnection.IceConnectionState.DISCONNECTED) {
                //JSONObject payload = new JSONObject();
                //sendMessage("reserve-connect", payload);
                //setVideoChannelStatus(false);
                //setDataChannelStatus(false);
                //if (websocketClient.getReadyState() != WebSocket.READYSTATE.OPEN) {
                //removePeer();
                //mListener.onPeerLeave();
                //mRecoLiveEvent.onDisconnected();
                //mListener.onStatusChanged("DISCONNECTED");
            }
        }

        @Override
        public void onIceConnectionReceivingChange(boolean var1) {};

        @Override
        public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {}

        @Override
        public void onIceCandidate(final IceCandidate candidate) {
            try {
                JSONObject payload = new JSONObject();
                payload.put("sdpMLineIndex", candidate.sdpMLineIndex);
                payload.put("sdpMid", candidate.sdpMid);
                payload.put("candidate", candidate.sdp);
                sendMessage("candidate", payload);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onIceCandidatesRemoved(IceCandidate[] var1) {};

        @Override
        public void onAddStream(MediaStream mediaStream) {
            Log.d(TAG,"onAddStream "+mediaStream.label());
            // remote streams are displayed from 1 to MAX_PEER (0 is localStream)
            mListener.onAddRemoteStream(mediaStream);
        }

        @Override
        public void onRemoveStream(MediaStream mediaStream) {
            Log.d(TAG,"onRemoveStream "+mediaStream.label());
            //removePeer();
            mListener.onRemoveRemoteStream();
        }

        @Override
        public void onDataChannel(DataChannel dc) {dataChannelLocal = dc;}

        @Override
        public void onRenegotiationNeeded() {}

        @Override
        public void onAddTrack(RtpReceiver var1, MediaStream[] var2) {}

        /* implement DataChannel.Observer interface */
        @Override
        public void onBufferedAmountChange(long var1) {
            Log.d(TAG, "onBufferedAmountChange: " + var1);
        }

        @Override
        public void onStateChange() {
            Log.d(TAG, "onStateChange: " + dataChannelLocal.state());
            if (dataChannelLocal.state() == DataChannel.State.OPEN &&
                    isUploadFile && getDataChannelStatus()) {
                Log.d(TAG, "sendData");
                executorSaveData.execute(new Runnable() {
                    @Override
                    public void run() {
                        sendData();
                    }
                });
            }
        }

        /**
         * A data buffer was successfully received.  NOTE: |buffer.data| will be
         * freed once this function returns so callers who want to use the data
         * asynchronously must make sure to copy it first.
         */
        @Override
        public void onMessage(DataChannel.Buffer buffer) {
            Log.d(TAG, "onMessage" + buffer.data.capacity());
            if(!isDownloadFile && getDataChannelStatus()) {
                return;
            }
            final ByteBuffer clone = ByteBuffer.allocate(buffer.data.capacity());
            buffer.data.rewind();   //copy from the beginning
            clone.put(buffer.data);
            buffer.data.rewind();
            executorSaveData.execute(new Runnable() {
                @Override
                public void run() {
                    saveData(clone);
                }
            });
        }

        public Peer() {
            Log.d(TAG,"new Peer: " + peerName);
            mListener.onStatusChanged("CONNECTING");
        }

        private String preferCodec(String sdpDescription, String codec, boolean isAudio) {
            String[] lines = sdpDescription.split("\r\n");
            int mLineIndex = -1;
            String codecRtpMap = null;
            // a=rtpmap:<payload type> <encoding name>/<clock rate> [/<encoding parameters>]
            String regex = "^a=rtpmap:(\\d+) " + codec + "(/\\d+)+[\r]?$";
            Pattern codecPattern = Pattern.compile(regex);
            String mediaDescription = "m=video ";
            if (isAudio) {
                mediaDescription = "m=audio ";
            }
            for (int i = 0; (i < lines.length)
                    && (mLineIndex == -1 || codecRtpMap == null); i++) {
                if (lines[i].startsWith(mediaDescription)) {
                    mLineIndex = i;
                    continue;
                }
                Matcher codecMatcher = codecPattern.matcher(lines[i]);
                if (codecMatcher.matches()) {
                    codecRtpMap = codecMatcher.group(1);
                }
            }
            if (codecRtpMap == null || mLineIndex == -1) {
                Log.w(TAG, "No rtpmap for " + codec);
                return "";
                //return sdpDescription;
            }
            Log.d(TAG, "Found " +  codec + " rtpmap " + codecRtpMap + ", prefer at "
                    + lines[mLineIndex]);
            String[] origMLineParts = lines[mLineIndex].split(" ");
            if (origMLineParts.length > 3) {
                StringBuilder newMLine = new StringBuilder();
                int origPartIndex = 0;
                // Format is: m=<media> <port> <proto> <fmt> ...
                newMLine.append(origMLineParts[origPartIndex++]).append(" ");
                newMLine.append(origMLineParts[origPartIndex++]).append(" ");
                newMLine.append(origMLineParts[origPartIndex++]).append(" ");
                newMLine.append(codecRtpMap);
                for (; origPartIndex < origMLineParts.length; origPartIndex++) {
                    if (!origMLineParts[origPartIndex].equals(codecRtpMap)) {
                        newMLine.append(" ").append(origMLineParts[origPartIndex]);
                    }
                }
                lines[mLineIndex] = newMLine.toString();
                Log.d(TAG, "Change media description: " + lines[mLineIndex]);
            } else {
                Log.e(TAG, "Wrong SDP media description format: " + lines[mLineIndex]);
            }
            StringBuilder newSdpDescription = new StringBuilder();
            for (String line : lines) {
                newSdpDescription.append(line).append("\r\n");
            }
            return newSdpDescription.toString();
        }
    }

    private long getTimeStamp(String host_add) {
        BufferedReader in = null;
        try {
            URL url = new URL("http://" + host_add);
            URLConnection urlConnection = url.openConnection();
            urlConnection.setRequestProperty("accept", "*/*");
            urlConnection.setRequestProperty("connection", "Keep-Alive");
            urlConnection.setRequestProperty("user-agent",
                    "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.8.1.14)");
            urlConnection.connect();
            in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            String result = "";
            while (null != (line = in.readLine())) {
                result += line;
            }
            Log.d(TAG, "getTimeStamp: " + result);
            String returnLable = "timestamp_is_";
            String timeStampString = result.substring(
                        result.indexOf(returnLable) + returnLable.length(), result.length());
            return Long.parseLong(timeStampString);
        } catch (Exception e) {
            Log.e(TAG, "发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        // http get request failed when go here, using system timestamp
        return getSystemTimestamp();
    }

    public static String hmacSha1Encrypt(String value, String key) throws Exception {
        byte[] keyBytes = key.getBytes();
        SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(signingKey);
        byte[] rawHmac = mac.doFinal(value.getBytes());
        return new String(Base64.encodeToString(rawHmac, Base64.NO_WRAP));
    }

    private void sendLoginSignal() {
        try {
            final JSONObject payload = new JSONObject();
            payload.put("type", "login");
            payload.put("name", userName);
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    websocketClient.send(payload.toString());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void login() {
        if (!loginInSuccess){
            sendLoginSignal();
        } else {
            Log.d(TAG,"logined alreadly");
        }
    }

    public void init(final RtcListener listener, final String socketAddress, String ip,
                     String peerName, RecoLiveEvent mRecoLiveEvent){
        cleanFileTransformParameter();
        this.host = ip;
        this.peerName = peerName;
        this.mListener = listener;
        this.mRecoLiveEvent = mRecoLiveEvent;
        setVideoChannelStatus(false);
        setDataChannelStatus(false);
        timestampWebrtc = getSystemTimestamp();

        iceServers = new LinkedList<>();
        pcConstraints = new MediaConstraints();
        pcConstraints.mandatory.add(
                new MediaConstraints.KeyValuePair("OfferToReceiveAudio", "false"));
        pcConstraints.mandatory.add(
                new MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"));
        pcConstraints.optional.add(
                new MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"));

        executor.execute(new Runnable() {
            @Override
            public void run() {
                if(factory == null) {
                    PeerConnectionFactory.initializeFieldTrials("");
                    PeerConnectionFactory.initializeAndroidGlobals(mListener, true, true, true);
                    PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();
                    factory = new PeerConnectionFactory(options);
                    peer = new Peer();
                    Random rand = new Random();
                    int n = rand.nextInt(100);
                    userName = Integer.toString(n);
                    //sync timestamp
                    long timeStamp = getTimeStamp(host + ":3033/timestamp");
                    String coturnUserName = Long.toString(timeStamp);
                    coturnUserName += (":" + userName);
                    String password = "";
                    try {
                        password = hmacSha1Encrypt(coturnUserName, turn_key);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String server_url = "turn:" + host;
                    Log.d(TAG, coturnUserName + password);
                    iceServers.add(new PeerConnection.IceServer(server_url + ":3478?transport=udp",
                            coturnUserName, password));
                    iceServers.add(new PeerConnection.IceServer(server_url + ":3478?transport=tcp",
                            coturnUserName, password));
                    iceServers.add(new PeerConnection.IceServer(server_url + ":3479?transport=udp",
                            coturnUserName, password));
                    iceServers.add(new PeerConnection.IceServer(server_url + ":3479?transport=tcp",
                            coturnUserName, password));
                }

                try {
                    if (websocketClient == null) {
                        websocketClient =
                                new WebSocketClient(new URI(socketAddress), new Draft_17()) {
                            public void onMessage(String message) {
                                System.out.println("got: " + message + "\n");
                                try {
                                    JSONObject data = new JSONObject(message);
                                    getMessage(data);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onOpen(ServerHandshake handshake) {
                                System.out.println("You are connected to ChatServer: " +
                                        getURI() + "\n");
                            }

                            @Override
                            public void onClose(int code, String reason, boolean remote) {
                                System.out.println("You have been disconnected from: " + getURI() +
                                        "; Code: " + code + " " + reason + "\n");
                            }

                            @Override
                            public void onError(Exception ex) {
                                System.out.println("Exception occured ...\n" + ex + "\n");
                                ex.printStackTrace();
                            }
                        };
                        websocketClient.connect();
                    }
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                try {
                    int timeout = 0;
                    while(!isConnected() && timeout < 6) {
                        Thread.sleep(500);
                        timeout =+ 1;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d(TAG,"here1");

                if(isConnected()) {
                    Log.d(TAG,"here2");

                    login();
                }
            }
        });
    }

    public void unInit() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                loginInSuccess = false;
                websocketClient.close();
                websocketClient = null;
                mRecoLiveEvent.onLogout();
                cleanFileTransformParameter();
                if (pc != null) {
                    pc.dispose();
                    pc = null;
                }
                peer = null;
                if (factory != null) {
                    factory.dispose();
                    factory = null;
                }
                pcConstraints = null;
                iceServers = null;
                setVideoChannelStatus(false);
                setDataChannelStatus(false);
            }
        });
    }

    public static WebRtcClient getInstance() {
        return instance;
    }

    private WebRtcClient() {
        // Executor thread is started once in private ctor and is used for all
        // peer connection API calls to ensure new peer connection factory is
        // created on the same thread as previously destroyed factory.
        executor = Executors.newSingleThreadScheduledExecutor();
        executorSaveData = Executors.newSingleThreadScheduledExecutor();
        executorPing = Executors.newSingleThreadScheduledExecutor();
    }

    public void startWebrtc(PeerConnectionParameters params, String peerName,
                            boolean videoChannelStart, boolean dataChannelStart) {
        Log.d(TAG, videoChannelStart + " : " + dataChannelStart);
        if(!videoChannelStart && !dataChannelStart) {
            Log.d(TAG, "startWebrtc error: videoChannelStart dataChannelStart both false");
            return;
        }
        if(videoChannelStart) {
            setVideoChannelStatus(videoChannelStart);
        }
        if(dataChannelStart) {
            Log.d(TAG, videoChannelStart + " : " + dataChannelStart);
            setDataChannelStatus(dataChannelStart);
        }
        this.peerName = peerName;
        if(createWebrtcCalled) {
            return;
        }
        createWebrtcCalled = true;
        cleanFileTransformParameter();
        peerConnectionParameters = params;

        executor.execute(new Runnable() {
            @Override
            public void run() {
                pc = factory.createPeerConnection(iceServers, pcConstraints, peer);
                //pc.addStream(localMediaStream);
                DataChannel.Init dataChannelInit = new DataChannel.Init();
                dataChannelInit.id = 1;
                dataChannelLocal = pc.createDataChannel("data_channel", dataChannelInit);
                dataChannelLocal.registerObserver(peer);
            }
        });
    }

    public void stopWebrtc(boolean videoChannelStop, boolean dataChannelStop) {
        if(!createWebrtcCalled) { return; }

        if(videoChannelStop) {
            setVideoChannelStatus(!videoChannelStop);
        }
        if(dataChannelStop) {
            setDataChannelStatus(!dataChannelStop);
        }
    }

    private void saveData(ByteBuffer data) {
        try {
            //Log.d(TAG, "saveData:downloadFileSizeCurrent " + downloadFileSizeCurrent);
            String[] filePathSplit = downloadFileName.split("\\.");
            String[] fileNameExtend = new String[2];
            if (filePathSplit.length == 2) {
                fileNameExtend = filePathSplit;
            } else {
                fileNameExtend[0] = downloadFileName;
                fileNameExtend[1] = "";
            }
            File file;
            if (downloadFileSizeCurrent == 0) {
                uploadRealFileSavePath = downloadSavePath + downloadFileName;
                file = new File(uploadRealFileSavePath);
                int i = 0;
                while (file.exists()){
                    uploadRealFileSavePath =
                            downloadSavePath + fileNameExtend[0] + "_" + i + "." + fileNameExtend[1];
                    file = new File(uploadRealFileSavePath);
                    i++;
                }
                file.createNewFile();
                Log.d(TAG, "uploadRealFileSavePath: " + uploadRealFileSavePath);
            } else {
                Log.d(TAG, "uploadRealFileSavePath: " + uploadRealFileSavePath);
                file = new File(uploadRealFileSavePath);
            }
            data.flip();
            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
            FileChannel channel = fileOutputStream.getChannel();
            channel.write(data);
            fileOutputStream.close();

            downloadFileSizeCurrent += data.limit();
            Log.d(TAG, "saveData:downloadFileSizeCurrent " + downloadFileSizeCurrent);
            mRecoLiveEvent.onFileProgress(uploadRealFileSavePath, downloadFileSize, downloadFileSizeCurrent);
            if (downloadFileSizeCurrent >= downloadFileSize) {
                mRecoLiveEvent.onFileFinish();
                JSONObject payload = new JSONObject();
                sendMessage("save-data-over", payload);
                cleanFileTransformParameter();
                setDataChannelStatus(false);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendData() {
        try {
            Log.d(TAG, "sendData" + uploadFilePath + ": " + uploadFileName + ": " +uploadFileSize);
            RandomAccessFile file = new RandomAccessFile(uploadFilePath + uploadFileName, "r");
            FileChannel inChannel = file.getChannel();
            int chunkSize = 1024 * 16;
            int chunkNum = 0;
            ByteBuffer buffer = ByteBuffer.allocate(chunkSize);
            while(inChannel.read(buffer) > 0) {
                buffer.flip();
                DataChannel.Buffer data = new DataChannel.Buffer(buffer, true);
                if(!isUploadFile) {
                    break;
                }
                while(dataChannelLocal.bufferedAmount() > chunkSize * 300) {
                    Thread.sleep(1000);
                }
                boolean sendResult = dataChannelLocal.send(data);
                Log.d(TAG, "sendData" + sendResult);
                buffer.clear();
                if((chunkNum % 5 == 0) && getVideoChannelStatus()) {
                    Log.d(TAG, "sleep: " + getVideoChannelStatus());
                    Thread.sleep(1000);
                }
                chunkNum += 1;
            }
            inChannel.close();
            while(!uploadFileAccepted) {Thread.sleep(500);}
            setDataChannelStatus(false);
            cleanFileTransformParameter();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }  catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "sendData" + "over");
    }
}
