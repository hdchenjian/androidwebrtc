package com.reconova.widget;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class RecoAutoCompleteTextView extends AutoCompleteTextView {
	public final static String  DEFAULT_PREFS = "RecoHistoryPrefs";
	private String mPrefsName = DEFAULT_PREFS;
	private String mHistorykey = "";
	private SharedPreferences mSettings;
	private List<String> mHistory;
	private int mMaxLines = 10;

	public RecoAutoCompleteTextView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public RecoAutoCompleteTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public RecoAutoCompleteTextView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 使用默认的preference来保存key所对应的需要Auto Complete的内容。
	 * @param historyKey
	 */
	public void initSourePreference(Activity activity, String historyKey) {
		initAutoComplete(activity, DEFAULT_PREFS, historyKey);
	}

	/**
	 * @param preference 配置文件名称
	 * @param historyKey 配置文件对应的key
	 */
	public void initAutoComplete(final Activity activity, String prefsName, String historyKey) {
		mPrefsName = prefsName;
		mHistorykey = historyKey;
		mSettings = getContext().getSharedPreferences(mPrefsName, 0);
		setOnKeyListener(new View.OnKeyListener() {

			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// If the event is a key-down event on the "enter" button
				if ((event.getAction() == KeyEvent.ACTION_DOWN)
						&& (keyCode == KeyEvent.KEYCODE_ENTER)) {
					addSearchInput(getText().toString());
					View view = activity.getCurrentFocus();
					if (view != null) {
						InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
					}

					return true;
				}
				return false;
			}

		});
		
		setHistoryData();
		setAutoCompleteSource();
	}
	
	public void showHistoryText() {
		if (!mHistory.isEmpty()) {
			setText(mHistory.get(0));
		} else if (getText().length() > 0){
			saveHistory();
		}
	}

	public void saveHistory() {
		String text = getText().toString();
		if (!text.isEmpty()) {
			addSearchInput(text);
		}
	}

	private void setHistoryData() {
		mHistory = new LinkedList<String>();
		String linestr = mSettings.getString(mHistorykey, "");
		String[] lines = linestr.split("\n");
		if (lines != null) {
			for (String line : lines) {
				if (!line.isEmpty()) {
					mHistory.add(line);
				}
				
			}
		}
	}

	private void setAutoCompleteSource() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
				android.R.layout.simple_spinner_dropdown_item,
				mHistory.toArray(new String[mHistory.size()]));
		setAdapter(adapter);
	}

	private void addSearchInput(String input) {
		boolean changed = false;
		if (!mHistory.contains(input)) {
			mHistory.add(0, input);
			changed = true;
		} else {
			// 将最近 输入的放在最上面。
			if (!mHistory.isEmpty() && !mHistory.get(0).equals(input)) {
				mHistory.remove(input);
				mHistory.add(0, input);
				changed = true;
			}
		}

		if (mHistory.size() > mMaxLines) {
			mHistory.remove(mHistory.size() - 1);
			changed = true;
		}
		if (changed) {
			savePrefs();
			setAutoCompleteSource();
		}
	}

	private void savePrefs() {
		SharedPreferences.Editor editor = mSettings.edit();
		editor.putString(mHistorykey, getLinesStr());
		editor.commit();
	}

	private String getLinesStr() {
		String linesStr = "";
		for (String line : mHistory) {
			line += "\n";
			linesStr += line;
		}
		if (linesStr.startsWith("\n")) {
			linesStr.replaceFirst("\n", "");
		}
		return linesStr;
	}

}
